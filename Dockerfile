FROM openjdk:11.0.13-jre-slim
VOLUME /tmp
ADD ./build/libs/company-demo-0.0.1-SNAPSHOT.jar company-demo.jar
ENTRYPOINT ["java", "-jar" , "/company-demo.jar"]