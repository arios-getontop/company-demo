case "$1" in
  (dev|qa|prod)
    echo "Starting deployment"
    git pull

    CID=$(docker ps -aqf "name=company-demo-server")
    echo $CID

    echo "Stopping containers"
    docker stop $CID

    echo "Removing containers"
    docker rm $CID

    echo "Removing image"
    docker rmi company-demo-server:v1

    echo "Building project"
    gradle build -x test

    echo "Building image and starting containers"
    docker build -t company-demo-server:v1 .
    docker run -p 8505:8505 -e SPRING_PROFILES_ACTIVE=$1  --network top-net --name company-demo-server -d company-demo-server:v1

    echo "Deployment finished"
  ;;
  (*) echo "Unknown environment. Possible values can be dev, qa, prod." ;;
esac
