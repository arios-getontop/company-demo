package com.top.wallet.application.usecase

import com.top.wallet.application.exception.EntityConflictException
import com.top.wallet.application.exception.RepositoryNotAvailableException
import com.top.wallet.application.port.in.CreateCompanyCommand
import com.top.wallet.application.port.out.CompanyRepository
import com.top.wallet.domain.Company
import spock.lang.Specification

class CreateCompanyUseCaseTest extends Specification {

    private static final int COMPANY_ID = 1121
    private static final String USER_ID = "16a5dd32-3d2a-4aa5-9980-6b3e2de305a0"
    private static final String NAME = "Magios"

    CompanyRepository companyRepository = Mock(CompanyRepository)

    CreateCompanyUseCase target = new CreateCompanyUseCase(companyRepository)

    def "given a company creation data when CreateCompanyUseCase is executed then repository is called correctly and response is as expected"() {
        given:
        def data = new CreateCompanyCommand.Data(USER_ID, NAME)
        def aCompanyToCreate = new Company(null, USER_ID, NAME)
        def expected = new Company(COMPANY_ID, USER_ID, NAME)

        when:
        def response = target.execute(data)

        then:
        1 * companyRepository.save(aCompanyToCreate) >> expected
        response == expected
    }

    def "given a company creation data when CreateCompanyUseCase is executed and the repository throws {exception} then {expected} is thrown"() {
        given:
        def data = new CreateCompanyCommand.Data(USER_ID, NAME)
        companyRepository.save(_ as Company) >> { throw exception }

        when:
        target.execute(data)

        then:
        thrown(expected)

        where:
        expected                        | exception
        EntityConflictException         | new EntityConflictException()
        RepositoryNotAvailableException | new RepositoryNotAvailableException()
        Exception                       | new Exception()
    }
}
