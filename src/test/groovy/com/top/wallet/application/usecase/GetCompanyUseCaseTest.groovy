package com.top.wallet.application.usecase

import com.top.wallet.application.exception.EntityNotFoundException
import com.top.wallet.application.exception.RepositoryNotAvailableException
import com.top.wallet.application.port.in.GetCompanyQuery
import com.top.wallet.application.port.out.CompanyRepository
import com.top.wallet.domain.Company
import spock.lang.Specification

class GetCompanyUseCaseTest extends Specification {

    private static final int COMPANY_ID = 1121
    private static final String USER_ID = "16a5dd32-3d2a-4aa5-9980-6b3e2de305a0"
    private static final String NAME = "Magios"

    CompanyRepository companyRepository = Mock(CompanyRepository)

    GetCompanyUseCase target = new GetCompanyUseCase(companyRepository)

    def "given a company id when GetCompanyUseCase is executed then repository is called correctly and response is as expected"() {
        given:
        def data = new GetCompanyQuery.Data(COMPANY_ID)
        def expected = new Company(COMPANY_ID, USER_ID, NAME)

        when:
        def response = target.execute(data)

        then:
        1 * companyRepository.findById(COMPANY_ID) >> expected
        response == expected
    }

    def "given any filters when GetCompanyUseCase is executed and the repository throws {exception} then {expected} is thrown"() {
        given:
        def data = new GetCompanyQuery.Data(COMPANY_ID)
        companyRepository.findById(_ as Integer) >> { throw exception }

        when:
        target.execute(data)

        then:
        thrown(expected)

        where:
        expected                        | exception
        EntityNotFoundException         | new EntityNotFoundException()
        RepositoryNotAvailableException | new RepositoryNotAvailableException()
        Exception                       | new Exception()
    }
}
