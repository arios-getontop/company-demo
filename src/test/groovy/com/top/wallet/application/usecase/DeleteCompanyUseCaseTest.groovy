package com.top.wallet.application.usecase

import com.top.wallet.application.exception.EntityNotFoundException
import com.top.wallet.application.exception.RepositoryNotAvailableException
import com.top.wallet.application.port.in.DeleteCompanyCommand
import com.top.wallet.application.port.out.CompanyRepository
import spock.lang.Specification

class DeleteCompanyUseCaseTest extends Specification {

    private static final int COMPANY_ID = 1121

    CompanyRepository companyRepository = Mock(CompanyRepository)

    DeleteCompanyUseCase target = new DeleteCompanyUseCase(companyRepository)

    def "given a company id when DeleteCompanyUseCase is executed then repository is called correctly"() {
        given:
        def data = new DeleteCompanyCommand.Data(COMPANY_ID)

        when:
        target.execute(data)

        then:
        1 * companyRepository.deleteById(COMPANY_ID)
    }

    def "given a company id when DeleteCompanyUseCase is executed and the repository throws EntityNotFoundException then no exception is thrown"() {
        given:
        def data = new DeleteCompanyCommand.Data(COMPANY_ID)
        companyRepository.deleteById(_ as Integer) >> { throw new EntityNotFoundException() }

        when:
        target.execute(data)

        then:
        noExceptionThrown()
    }

    def "given a company id when DeleteCompanyUseCase is executed and the repository throws {exception} then {expected} is thrown"() {
        given:
        def data = new DeleteCompanyCommand.Data(COMPANY_ID)
        companyRepository.deleteById(_ as Integer) >> { throw exception }

        when:
        target.execute(data)

        then:
        thrown(expected)

        where:
        expected                        | exception
        RepositoryNotAvailableException | new RepositoryNotAvailableException()
        Exception                       | new Exception()
    }
}
