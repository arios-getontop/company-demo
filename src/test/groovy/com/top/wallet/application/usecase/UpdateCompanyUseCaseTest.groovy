package com.top.wallet.application.usecase

import com.top.wallet.application.exception.EntityConflictException
import com.top.wallet.application.exception.EntityNotFoundException
import com.top.wallet.application.exception.RepositoryNotAvailableException
import com.top.wallet.application.port.in.UpdateCompanyCommand
import com.top.wallet.application.port.out.CompanyRepository
import com.top.wallet.domain.Company
import spock.lang.Specification

class UpdateCompanyUseCaseTest extends Specification {

    private static final int COMPANY_ID = 1121
    private static final String USER_ID = "16a5dd32-3d2a-4aa5-9980-6b3e2de305a0"
    private static final String NAME = "Magios"

    CompanyRepository companyRepository = Mock(CompanyRepository)

    UpdateCompanyUseCase target = new UpdateCompanyUseCase(companyRepository)

    def "given a company update data when UpdateCompanyUseCase is executed then repository is called correctly and response is as expected"() {
        given:
        def data = new UpdateCompanyCommand.Data(COMPANY_ID, USER_ID, null)
        def aCompanyToUpdate = new Company(COMPANY_ID, USER_ID, null)
        def expected = new Company(COMPANY_ID, USER_ID, NAME)

        when:
        def response = target.execute(data)

        then:
        1 * companyRepository.update(aCompanyToUpdate) >> expected
        response == expected
    }

    def "given a company update data when UpdateCompanyUseCase is executed and the repository throws {exception} then {expected} is thrown"() {
        given:
        def data = new UpdateCompanyCommand.Data(COMPANY_ID, USER_ID, null)
        companyRepository.update(_ as Company) >> { throw exception }

        when:
        target.execute(data)

        then:
        thrown(expected)

        where:
        expected                        | exception
        EntityNotFoundException         | new EntityNotFoundException()
        EntityConflictException         | new EntityConflictException()
        RepositoryNotAvailableException | new RepositoryNotAvailableException()
        Exception                       | new Exception()
    }
}
