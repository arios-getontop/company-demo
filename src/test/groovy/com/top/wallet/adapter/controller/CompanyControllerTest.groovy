package com.top.wallet.adapter.controller

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.top.wallet.adapter.controller.model.*
import com.top.wallet.application.exception.EntityConflictException
import com.top.wallet.application.exception.EntityNotFoundException
import com.top.wallet.application.exception.InvalidFiltersException
import com.top.wallet.application.exception.RepositoryNotAvailableException
import com.top.wallet.application.port.in.*
import com.top.wallet.config.ErrorCode
import com.top.wallet.config.ErrorHandler
import com.top.wallet.domain.Company
import com.top.wallet.domain.Page
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup

class CompanyControllerTest extends Specification {

    static ObjectMapper objectMapper = new ObjectMapper()
    private static final int COMPANY_ID = 1121
    private static final String USER_ID = "16a5dd32-3d2a-4aa5-9980-6b3e2de305a0"
    private static final String NAME = "Magios"

    GetCompanyQuery getCompanyQuery = Mock(GetCompanyQuery)
    CreateCompanyCommand createCompanyCommand = Mock(CreateCompanyCommand)
    GetCompaniesListQuery getCompaniesListQuery = Mock(GetCompaniesListQuery)
    GetCompaniesPageQuery getCompaniesPageQuery = Mock(GetCompaniesPageQuery)
    UpdateCompanyCommand updateCompanyCommand = Mock(UpdateCompanyCommand)
    PatchCompanyCommand patchCompanyCommand = Mock(PatchCompanyCommand)
    DeleteCompanyCommand deleteCompanyCommand = Mock(DeleteCompanyCommand)

    CompanyController target = new CompanyController(
            getCompanyQuery,
            createCompanyCommand,
            getCompaniesListQuery,
            getCompaniesPageQuery,
            updateCompanyCommand,
            patchCompanyCommand,
            deleteCompanyCommand
    )

    MockMvc mvc = standaloneSetup(target)
            .setControllerAdvice(new ErrorHandler())
            .build()

    def "given a company id when a GET is performed to /api/companies then GetCompanyQuery is executed correctly and the response content is as expected and the response status is 200"() {
        given:
        def data = new GetCompanyQuery.Data(COMPANY_ID)
        def expected = new CompanyRestModel(COMPANY_ID, USER_ID, NAME)
        def aCompany = new Company(COMPANY_ID, USER_ID, NAME)

        when:
        def response = mvc.perform(get("/api/companies/" + COMPANY_ID)
                .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .response

        then:
        1 * getCompanyQuery.execute(data) >> aCompany
        response.status == 200
        objectMapper.readValue(response.contentAsString, CompanyRestModel) == expected
    }

    def "given an invalid company id when a GET is performed to /api/companies then GetCompanyQuery is not executed and 400 is answered"() {
        when:
        def response = mvc.perform(get("/api/companies/1a")
                .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .response
        then:
        response.status == 400
        0 * getCompanyQuery.execute(_)
    }

    def "given a company id when a GET is performed to /api/companies and GetCompanyQuery throws {exception} then {status} is answered with the expected message"() {
        given:
        getCompanyQuery.execute(_ as GetCompanyQuery.Data) >> { throw exception }

        when:
        def response = mvc.perform(get("/api/companies/" + COMPANY_ID)
                .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .response
        then:
        response.status == status
        response.contentAsString.contains(expectedMessage)

        where:
        status | exception                                                                       | expectedMessage
        404    | new EntityNotFoundException(ErrorCode.COMPANY_NOT_FOUND)                        | ErrorCode.COMPANY_NOT_FOUND.name()
        500    | new RuntimeException("Unexpected Error")                                        | ""
        503    | new RepositoryNotAvailableException(ErrorCode.COMPANY_REPOSITORY_NOT_AVAILABLE) | ErrorCode.COMPANY_REPOSITORY_NOT_AVAILABLE.name()
    }

    def "given a company when a POST is performed to /api/companies then CreateCompanyCommand is executed correctly and the response content is as expected and the response status is 201"() {
        given:
        def data = new CreateCompanyCommand.Data(USER_ID, NAME)
        def request = new CreateCompanyRequestBody(USER_ID, NAME)
        def expected = new CompanyRestModel(COMPANY_ID, USER_ID, NAME)
        def aCompany = new Company(COMPANY_ID, USER_ID, NAME)

        when:
        def response = mvc.perform(post("/api/companies/")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andReturn()
                .response

        then:
        1 * createCompanyCommand.execute(data) >> aCompany
        response.status == 201
        objectMapper.readValue(response.contentAsString, CompanyRestModel) == expected
    }

    def "given an invalid request when a POST is performed to /api/companies then CreateCompanyCommand is not executed and 400 is answered with the expected message"() {
        when:
        def response = mvc.perform(post("/api/companies/")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andReturn()
                .response
        then:
        response.status == 400
        response.contentAsString.contains(expectedMessage)
        0 * createCompanyCommand.execute(_)

        where:
        request                                                                     | expectedMessage
        new CreateCompanyRequestBody(null, null)                                    | objectMapper.writeValueAsString([user_id: ErrorCode.INVALID_USER_ID.name(), name: ErrorCode.INVALID_NAME.name()])
        new CreateCompanyRequestBody("16a5dd32-3d2a-4aa5-9980-6b3e2de305a", NAME)   | objectMapper.writeValueAsString([user_id: ErrorCode.INVALID_USER_ID.name()])
        new CreateCompanyRequestBody("16a5dd32-3d2a-4aa5-9980-6b3e2de305a11", NAME) | objectMapper.writeValueAsString([user_id: ErrorCode.INVALID_USER_ID.name()])
        new CreateCompanyRequestBody("16a5dd32-3d2a-4aa5-9980-6b3e2de305g0", NAME)  | objectMapper.writeValueAsString([user_id: ErrorCode.INVALID_USER_ID.name()])
        new CreateCompanyRequestBody("16a5dd323d2a4aa599806b3e2de305a", NAME)       | objectMapper.writeValueAsString([user_id: ErrorCode.INVALID_USER_ID.name()])
        new CreateCompanyRequestBody(USER_ID, "AB")                                 | objectMapper.writeValueAsString([name: ErrorCode.INVALID_NAME.name()])
        new CreateCompanyRequestBody(USER_ID, "A" * 256)                            | objectMapper.writeValueAsString([name: ErrorCode.INVALID_NAME.name()])
    }

    def "given a company when a POST is performed to /api/companies and CreateCompanyCommand throws {exception} then {status} is answered with the expected message"() {
        given:
        def request = new CreateCompanyRequestBody(USER_ID, NAME)
        createCompanyCommand.execute(_ as CreateCompanyCommand.Data) >> { throw exception }

        when:
        def response = mvc.perform(post("/api/companies/")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andReturn()
                .response

        then:
        response.status == status
        response.contentAsString.contains(expectedMessage)

        where:
        status | exception                                                                       | expectedMessage
        409    | new EntityConflictException(ErrorCode.COMPANY_ALREADY_EXISTS)                   | ErrorCode.COMPANY_ALREADY_EXISTS.name()
        500    | new RuntimeException("Unexpected Error")                                        | ""
        503    | new RepositoryNotAvailableException(ErrorCode.COMPANY_REPOSITORY_NOT_AVAILABLE) | ErrorCode.COMPANY_REPOSITORY_NOT_AVAILABLE.name()
    }

    def "given a filter when a GET is performed to /api/companies then GetCompaniesListQuery is executed correctly and the response content is as expected and the response status is 200"() {
        given:
        def data = new GetCompaniesListQuery.Data([user_id: "equals," + USER_ID])
        def expected = [new CompanyRestModel(COMPANY_ID, USER_ID, NAME)]
        def aCompany = [new Company(COMPANY_ID, USER_ID, NAME)]

        when:
        def response = mvc.perform(get("/api/companies?user_id=equals," + USER_ID)
                .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .response

        then:
        1 * getCompaniesListQuery.execute(data) >> aCompany
        response.status == 200
        objectMapper.readValue(response.contentAsString, new TypeReference<List<CompanyRestModel>>() {}) == expected
    }

    def "given a filter when a GET is performed to /api/companies and GetCompaniesListQuery throws {exception} then {status} is answered with the expected message"() {
        given:
        def request = new CreateCompanyRequestBody(USER_ID, NAME)
        getCompaniesListQuery.execute(_ as GetCompaniesListQuery.Data) >> { throw exception }

        when:
        def response = mvc.perform(get("/api/companies/")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andReturn()
                .response

        then:
        response.status == status
        response.contentAsString.contains(expectedMessage)


        where:
        status | exception                                                                       | expectedMessage
        400    | new InvalidFiltersException(ErrorCode.INVALID_FILTERS)                          | ErrorCode.INVALID_FILTERS.name()
        500    | new RuntimeException("Unexpected Error")                                        | ""
        503    | new RepositoryNotAvailableException(ErrorCode.COMPANY_REPOSITORY_NOT_AVAILABLE) | ErrorCode.COMPANY_REPOSITORY_NOT_AVAILABLE.name()
    }

    def "given a filter and a pagination when a GET is performed to /api/companies then GetCompaniesPageQuery is executed correctly and the response content is as expected and the response status is 200"() {
        given:
        def data = new GetCompaniesPageQuery.Data(0, 10, [user_id: "equals," + USER_ID])
        def expected = PageResponse.builder()
                .content([new CompanyRestModel(COMPANY_ID, USER_ID, NAME)])
                .totalElements(1)
                .size(1)
                .number(1)
                .totalPages(1)
                .build()
        def aCompany = Page.builder()
                .content([new Company(COMPANY_ID, USER_ID, NAME)])
                .totalElements(1)
                .size(1)
                .number(1)
                .totalPages(1)
                .build()

        when:
        def response = mvc.perform(get("/api/companies?user_id=equals," + USER_ID + "&page=0&size=10")
                .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .response

        then:
        1 * getCompaniesPageQuery.execute(data) >> aCompany
        response.status == 200
        objectMapper.readValue(response.contentAsString, new TypeReference<PageResponse<CompanyRestModel>>() {
        }) == expected
    }

    def "given a filter and a pagination when a GET is performed to /api/companies and GetCompaniesPageQuery throws {exception} then {status} is answered with the expected message"() {
        given:
        getCompaniesPageQuery.execute(_ as GetCompaniesPageQuery.Data) >> { throw exception }

        when:
        def response = mvc.perform(get("/api/companies?user_id=equals," + USER_ID + "&page=0&size=10")
                .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .response


        then:
        response.status == status
        response.contentAsString.contains(expectedMessage)

        where:
        status | exception                                                                       | expectedMessage
        400    | new InvalidFiltersException(ErrorCode.INVALID_FILTERS)                          | ErrorCode.INVALID_FILTERS.name()
        500    | new RuntimeException("Unexpected Error")                                        | ""
        503    | new RepositoryNotAvailableException(ErrorCode.COMPANY_REPOSITORY_NOT_AVAILABLE) | ErrorCode.COMPANY_REPOSITORY_NOT_AVAILABLE.name()
    }

    def "given a company and a company id when a PUT is performed to /api/companies then UpdateCompanyCommand is executed correctly and the response content is as expected and the response status is 200"() {
        given:
        def data = new UpdateCompanyCommand.Data(COMPANY_ID, USER_ID, NAME)
        def request = new UpdateCompanyRequestBody(USER_ID, NAME)
        def expected = new CompanyRestModel(COMPANY_ID, USER_ID, NAME)
        def aCompany = new Company(COMPANY_ID, USER_ID, NAME)

        when:
        def response = mvc.perform(put("/api/companies/" + COMPANY_ID)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andReturn()
                .response

        then:
        1 * updateCompanyCommand.execute(data) >> aCompany
        response.status == 200
        objectMapper.readValue(response.contentAsString, CompanyRestModel) == expected
    }

    def "given an invalid company id when a PUT is performed to /api/companies then UpdateCompanyQuery is not executed and 400 is answered"() {
        when:
        def response = mvc.perform(put("/api/companies/1a")
                .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .response
        then:
        response.status == 400
        0 * updateCompanyCommand.execute(_)
    }

    def "given an invalid request when a PUT is performed to /api/companies then UpdateCompanyCommand is not executed and 400 is answered with the expected message"() {
        when:
        def response = mvc.perform(put("/api/companies/" + COMPANY_ID)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andReturn()
                .response
        then:
        response.status == 400
        response.contentAsString.contains(expectedMessage)
        0 * updateCompanyCommand.execute(_)

        where:
        request                                                                     | expectedMessage
        new UpdateCompanyRequestBody(null, null)                                    | objectMapper.writeValueAsString([user_id: ErrorCode.INVALID_USER_ID.name(), name: ErrorCode.INVALID_NAME.name()])
        new UpdateCompanyRequestBody("16a5dd32-3d2a-4aa5-9980-6b3e2de305a", NAME)   | objectMapper.writeValueAsString([user_id: ErrorCode.INVALID_USER_ID.name()])
        new UpdateCompanyRequestBody("16a5dd32-3d2a-4aa5-9980-6b3e2de305a11", NAME) | objectMapper.writeValueAsString([user_id: ErrorCode.INVALID_USER_ID.name()])
        new UpdateCompanyRequestBody("16a5dd32-3d2a-4aa5-9980-6b3e2de305g0", NAME)  | objectMapper.writeValueAsString([user_id: ErrorCode.INVALID_USER_ID.name()])
        new UpdateCompanyRequestBody("16a5dd323d2a4aa599806b3e2de305a", NAME)       | objectMapper.writeValueAsString([user_id: ErrorCode.INVALID_USER_ID.name()])
        new UpdateCompanyRequestBody(USER_ID, "AB")                                 | objectMapper.writeValueAsString([name: ErrorCode.INVALID_NAME.name()])
        new UpdateCompanyRequestBody(USER_ID, "A" * 256)                            | objectMapper.writeValueAsString([name: ErrorCode.INVALID_NAME.name()])
    }

    def "given a company when a PUT is performed to /api/companies and UpdateCompanyCommand throws {exception} then {status} is answered with the expected message"() {
        given:
        def request = new UpdateCompanyRequestBody(USER_ID, NAME)
        updateCompanyCommand.execute(_ as UpdateCompanyCommand.Data) >> { throw exception }

        when:
        def response = mvc.perform(put("/api/companies/" + COMPANY_ID)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andReturn()
                .response

        then:
        response.status == status
        response.contentAsString.contains(expectedMessage)

        where:
        status | exception                                                                       | expectedMessage
        404    | new EntityNotFoundException(ErrorCode.COMPANY_NOT_FOUND)                        | ErrorCode.COMPANY_NOT_FOUND.name()
        409    | new EntityConflictException(ErrorCode.COMPANY_ALREADY_EXISTS)                   | ErrorCode.COMPANY_ALREADY_EXISTS.name()
        500    | new RuntimeException("Unexpected Error")                                        | ""
        503    | new RepositoryNotAvailableException(ErrorCode.COMPANY_REPOSITORY_NOT_AVAILABLE) | ErrorCode.COMPANY_REPOSITORY_NOT_AVAILABLE.name()
    }

    def "given a company and a company id when a PATCH is performed to /api/companies then PatchCompanyCommand is executed correctly and the response content is as expected and the response status is 200"() {
        given:
        def data = new PatchCompanyCommand.Data(COMPANY_ID, USER_ID, NAME)
        def request = new PatchCompanyRequestBody(USER_ID, NAME)
        def expected = new CompanyRestModel(COMPANY_ID, USER_ID, NAME)
        def aCompany = new Company(COMPANY_ID, USER_ID, NAME)

        when:
        def response = mvc.perform(patch("/api/companies/" + COMPANY_ID)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andReturn()
                .response

        then:
        1 * patchCompanyCommand.execute(data) >> aCompany
        response.status == 200
        objectMapper.readValue(response.contentAsString, CompanyRestModel) == expected
    }

    def "given an invalid company id when a PATCH is performed to /api/companies then PatchCompanyQuery is not executed and 400 is answered"() {
        when:
        def response = mvc.perform(patch("/api/companies/1a")
                .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .response
        then:
        response.status == 400
        0 * patchCompanyCommand.execute(_)
    }

    def "given an invalid request when a PATCH is performed to /api/companies then PatchCompanyCommand is not executed and 400 is answered with the expected message"() {
        when:
        def response = mvc.perform(patch("/api/companies/" + COMPANY_ID)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andReturn()
                .response
        then:
        response.status == 400
        response.contentAsString.contains(expectedMessage)
        0 * patchCompanyCommand.execute(_)

        where:
        request                                                                    | expectedMessage
        new PatchCompanyRequestBody("16a5dd32-3d2a-4aa5-9980-6b3e2de305a", null)   | objectMapper.writeValueAsString([user_id: ErrorCode.INVALID_USER_ID.name()])
        new PatchCompanyRequestBody("16a5dd32-3d2a-4aa5-9980-6b3e2de305a11", NAME) | objectMapper.writeValueAsString([user_id: ErrorCode.INVALID_USER_ID.name()])
        new PatchCompanyRequestBody("16a5dd32-3d2a-4aa5-9980-6b3e2de305g0", NAME)  | objectMapper.writeValueAsString([user_id: ErrorCode.INVALID_USER_ID.name()])
        new PatchCompanyRequestBody("16a5dd323d2a4aa599806b3e2de305a", NAME)       | objectMapper.writeValueAsString([user_id: ErrorCode.INVALID_USER_ID.name()])
        new PatchCompanyRequestBody(null, "AB")                                    | objectMapper.writeValueAsString([name: ErrorCode.INVALID_NAME.name()])
        new PatchCompanyRequestBody(USER_ID, "A" * 256)                            | objectMapper.writeValueAsString([name: ErrorCode.INVALID_NAME.name()])
    }

    def "given a company when a PATCH is performed to /api/companies and PatchCompanyCommand throws {exception} then {status} is answered with the expected message"() {
        given:
        def request = new PatchCompanyRequestBody(USER_ID, NAME)
        patchCompanyCommand.execute(_ as PatchCompanyCommand.Data) >> { throw exception }

        when:
        def response = mvc.perform(patch("/api/companies/" + COMPANY_ID)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andReturn()
                .response

        then:
        response.status == status
        response.contentAsString.contains(expectedMessage)

        where:
        status | exception                                                                       | expectedMessage
        404    | new EntityNotFoundException(ErrorCode.COMPANY_NOT_FOUND)                        | ErrorCode.COMPANY_NOT_FOUND.name()
        409    | new EntityConflictException(ErrorCode.COMPANY_ALREADY_EXISTS)                   | ErrorCode.COMPANY_ALREADY_EXISTS.name()
        500    | new RuntimeException("Unexpected Error")                                        | ""
        503    | new RepositoryNotAvailableException(ErrorCode.COMPANY_REPOSITORY_NOT_AVAILABLE) | ErrorCode.COMPANY_REPOSITORY_NOT_AVAILABLE.name()
    }

    def "given a company id when a DELETE is performed to /api/companies then DeleteCompanyCommand is executed correctly and the response content is as expected and the response status is 204"() {
        given:
        def data = new DeleteCompanyCommand.Data(COMPANY_ID)

        when:
        def response = mvc.perform(delete("/api/companies/" + COMPANY_ID)
                .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .response

        then:
        1 * deleteCompanyCommand.execute(data)
        response.status == 204
    }

    def "given an invalid company id when a DELETE is performed to /api/companies then DeleteCompanyCommand is not executed and 400 is answered"() {
        when:
        def response = mvc.perform(delete("/api/companies/1a")
                .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .response
        then:
        response.status == 400
        0 * deleteCompanyCommand.execute(_)
    }

    def "given a company id when a DELETE is performed to /api/companies and DeleteCompanyCommand throws {exception} then {status} is answered with the expected message"() {
        given:
        deleteCompanyCommand.execute(_ as DeleteCompanyCommand.Data) >> { throw exception }

        when:
        def response = mvc.perform(delete("/api/companies/" + COMPANY_ID)
                .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .response
        then:
        response.status == status
        response.contentAsString.contains(expectedMessage)

        where:
        status | exception                                                                       | expectedMessage
        500    | new RuntimeException("Unexpected Error")                                        | ""
        503    | new RepositoryNotAvailableException(ErrorCode.COMPANY_REPOSITORY_NOT_AVAILABLE) | ErrorCode.COMPANY_REPOSITORY_NOT_AVAILABLE.name()
    }
}
