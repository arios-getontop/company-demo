package com.top.wallet.adapter.jdbc

import com.top.wallet.adapter.jbdc.CompanyJdbcAdapter
import com.top.wallet.adapter.jbdc.model.CompanyJdbcModel
import com.top.wallet.application.exception.EntityConflictException
import com.top.wallet.application.exception.EntityNotFoundException
import com.top.wallet.application.exception.RepositoryNotAvailableException
import com.top.wallet.domain.Page
import com.top.wallet.domain.Company
import org.springframework.dao.DataAccessException
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.BeanPropertyRowMapper
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import spock.lang.Specification

class CompanyJdbcAdapterTest extends Specification {

    private static final int COMPANY_ID = 1121
    private static final String USER_ID = "16a5dd32-3d2a-4aa5-9980-6b3e2de305a0"
    private static final String NAME = "Magios"

    NamedParameterJdbcTemplate jdbcTemplate = Mock(NamedParameterJdbcTemplate)

    CompanyJdbcAdapter target = new CompanyJdbcAdapter(jdbcTemplate)

    def "given a company creation data when save is executed then jdbcTemplate is called correctly and response is as expected"() {
        given:
        def aCompanyToCreate = new Company(null, USER_ID, NAME)
        def expected = new Company(COMPANY_ID, USER_ID, NAME)

        when:
        def response = target.save(aCompanyToCreate)

        then:
        1 * jdbcTemplate.queryForObject({ it.contains("INSERT INTO companies") }, ["USER_ID": USER_ID, "NAME": NAME], Integer.class) >> COMPANY_ID
        response == expected
    }

    def "given a company creation data when save is executed and the jdbcTemplate throws {exception} then {expected} is thrown"() {
        given:
        def aCompanyToCreate = new Company(null, USER_ID, NAME)
        jdbcTemplate.queryForObject(_ as String, _ as Map, _ as Class) >> { throw exception }

        when:
        target.save(aCompanyToCreate)

        then:
        thrown(expected)

        where:
        expected                        | exception
        EntityConflictException         | Mock(DataIntegrityViolationException)
        RepositoryNotAvailableException | Mock(DataAccessException)
        RuntimeException                | new Exception()
    }

    def "given a company id when deleteById is executed then jdbcTemplate is called correctly"() {
        when:
        target.deleteById(COMPANY_ID)

        then:
        1 * jdbcTemplate.update({ it.contains("DELETE FROM companies") }, ["COMPANY_ID": COMPANY_ID])
    }

    def "given a company id when deleteById is executed and the jdbcTemplate throws {exception} then {expected} is thrown"() {
        given:
        jdbcTemplate.update(_ as String, _ as Map) >> { throw exception }

        when:
        target.deleteById(COMPANY_ID)

        then:
        thrown(expected)

        where:
        expected                        | exception
        EntityNotFoundException         | Mock(EmptyResultDataAccessException)
        RepositoryNotAvailableException | Mock(DataAccessException)
        RuntimeException                | new Exception()
    }

    def "given companies and its jdbcModel when findAll is executed then jdbcTemplate is called correctly and response is as expected"() {
        given:
        def aCompany = new Company(COMPANY_ID, USER_ID, NAME)
        def otherCompany = new Company(COMPANY_ID, USER_ID, NAME)
        def aCompanyJdbcModel = new CompanyJdbcModel(COMPANY_ID, USER_ID, NAME)
        def otherCompanyJdbcModel = new CompanyJdbcModel(COMPANY_ID, USER_ID, NAME)
        def expected = [aCompany, otherCompany]

        when:
        def response = target.findAll(Collections.emptyList())

        then:
        1 * jdbcTemplate.query({ it.contains("SELECT * FROM companies") }, Collections.emptyMap(), _ as BeanPropertyRowMapper) >> [aCompanyJdbcModel, otherCompanyJdbcModel]
        response == expected
    }

    def "given any filters when findAll is executed and the jdbcTemplate throws {exception} then {expected} is thrown"() {
        given:
        jdbcTemplate.query(_ as String, _ as Map, _ as RowMapper) >> { throw exception }

        when:
        target.findAll(Collections.emptyList())

        then:
        thrown(expected)

        where:
        expected                        | exception
        RepositoryNotAvailableException | Mock(DataAccessException)
        RuntimeException                | new Exception()
    }

    def "given a pagination, companies and its jdbcModel when findAll is executed then jdbcTemplate is called correctly and response is as expected"() {
        given:
        def aCompany = new Company(COMPANY_ID, USER_ID, NAME)
        def otherCompany = new Company(COMPANY_ID, USER_ID, NAME)
        def aCompanyJdbcModel = new CompanyJdbcModel(COMPANY_ID, USER_ID, NAME)
        def otherCompanyJdbcModel = new CompanyJdbcModel(COMPANY_ID, USER_ID, NAME)
        def expected = Page.builder()
                .content([aCompany, otherCompany])
                .totalElements(2)
                .size(10)
                .number(0)
                .totalPages(1)
                .build()

        when:
        def response = target.findAll(0, 10, Collections.emptyList())

        then:
        1 * jdbcTemplate.queryForObject({ it.contains("SELECT COUNT(*) FROM (SELECT * FROM companies") }, Collections.emptyMap(), Integer.class) >> 2
        1 * jdbcTemplate.query({ it.contains("SELECT * FROM companies") }, Collections.emptyMap(), _ as BeanPropertyRowMapper) >> [aCompanyJdbcModel, otherCompanyJdbcModel]
        response == expected
    }

    def "given any pagination and filters when findAll is executed and the jdbcTemplate throws {exception} then {expected} is thrown"() {
        given:
        jdbcTemplate.query(_ as String, _ as Map, _ as RowMapper) >> { throw exception }

        when:
        target.findAll(0, 10, Collections.emptyList())

        then:
        thrown(expected)
        1 * jdbcTemplate.queryForObject({ it.contains("SELECT COUNT(*) FROM (SELECT * FROM companies") }, Collections.emptyMap(), Integer.class) >> 2

        where:
        expected                        | exception
        RepositoryNotAvailableException | Mock(DataAccessException)
        RuntimeException                | new Exception()
    }

    def "given a company id when findById is executed then jdbcTemplate is called correctly and response is as expected"() {
        given:
        def aCompanyJdbcModel = new CompanyJdbcModel(COMPANY_ID, USER_ID, NAME)
        def expected = new Company(COMPANY_ID, USER_ID, NAME)

        when:
        def response = target.findById(COMPANY_ID)

        then:
        1 * jdbcTemplate.queryForObject({ it.contains("SELECT * FROM companies") }, ["COMPANY_ID": COMPANY_ID], _ as BeanPropertyRowMapper) >> aCompanyJdbcModel
        response == expected
    }

    def "given a company id when findById is executed and the jdbcTemplate throws {exception} then {expected} is thrown"() {
        given:
        jdbcTemplate.queryForObject(_ as String, _ as Map, _ as BeanPropertyRowMapper) >> { throw exception }

        when:
        target.findById(COMPANY_ID)

        then:
        thrown(expected)

        where:
        expected                        | exception
        EntityNotFoundException         | Mock(EmptyResultDataAccessException)
        RepositoryNotAvailableException | Mock(DataAccessException)
        RuntimeException                | new Exception()
    }

    def "given a company patch data when patch is executed then jdbcTemplate is called correctly and response is as expected"() {
        given:
        def aCompanyToPatch = new Company(COMPANY_ID, USER_ID, null)
        def aCompanyJdbcModel = new CompanyJdbcModel(COMPANY_ID, USER_ID, NAME)
        def expected = new Company(COMPANY_ID, USER_ID, NAME)

        when:
        def response = target.patch(aCompanyToPatch)

        then:
        1 * jdbcTemplate.queryForObject({ it.contains("UPDATE companies SET") }, ["COMPANY_ID": COMPANY_ID, "USER_ID": USER_ID, "NAME": null], _ as BeanPropertyRowMapper) >> aCompanyJdbcModel
        response == expected
    }

    def "given a company patch data when patch is executed and the jdbcTemplate throws {exception} then {expected} is thrown"() {
        given:
        def aCompanyToPatch = new Company(COMPANY_ID, USER_ID, null)
        jdbcTemplate.queryForObject(_ as String, _ as Map, _ as BeanPropertyRowMapper) >> { throw exception }

        when:
        target.patch(aCompanyToPatch)

        then:
        thrown(expected)

        where:
        expected                        | exception
        EntityNotFoundException         | Mock(EmptyResultDataAccessException)
        EntityConflictException         | Mock(DataIntegrityViolationException)
        RepositoryNotAvailableException | Mock(DataAccessException)
        RuntimeException                | new Exception()
    }

    def "given a company update data when patch is executed then jdbcTemplate is called correctly and response is as expected"() {
        given:
        def aCompanyToUpdate = new Company(COMPANY_ID, USER_ID, NAME)
        def aCompanyJdbcModel = new CompanyJdbcModel(COMPANY_ID, USER_ID, NAME)
        def expected = new Company(COMPANY_ID, USER_ID, NAME)

        when:
        def response = target.update(aCompanyToUpdate)

        then:
        1 * jdbcTemplate.update({ it.contains("UPDATE companies SET") }, ["COMPANY_ID": COMPANY_ID, "USER_ID": USER_ID, "NAME": NAME])
        response == expected
    }

    def "given a company update data when patch is executed and the jdbcTemplate throws {exception} then {expected} is thrown"() {
        given:
        def aCompanyToUpdate = new Company(COMPANY_ID, USER_ID, NAME)
        jdbcTemplate.update(_ as String, _ as Map) >> { throw exception }

        when:
        target.update(aCompanyToUpdate)

        then:
        thrown(expected)

        where:
        expected                        | exception
        EntityNotFoundException         | Mock(EmptyResultDataAccessException)
        EntityConflictException         | Mock(DataIntegrityViolationException)
        RepositoryNotAvailableException | Mock(DataAccessException)
        RuntimeException                | new Exception()
    }
}
