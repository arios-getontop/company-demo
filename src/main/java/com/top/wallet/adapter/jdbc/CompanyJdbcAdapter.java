package com.top.wallet.adapter.jdbc;

import com.top.wallet.adapter.jdbc.model.filters.JdbcFilter;
import com.top.wallet.adapter.jdbc.model.filters.JdbcFilterBuilder;
import com.top.wallet.adapter.jdbc.model.filters.JdbcQueryBuilder;
import com.top.wallet.application.exception.EntityConflictException;
import com.top.wallet.application.exception.EntityNotFoundException;
import com.top.wallet.application.exception.RepositoryNotAvailableException;
import com.top.wallet.domain.Page;
import com.top.wallet.domain.filters.Filter;
import com.top.wallet.adapter.jdbc.model.CompanyJdbcModel;
import com.top.wallet.application.port.out.CompanyRepository;
import com.top.wallet.config.ErrorCode;
import com.top.wallet.domain.Company;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

@Component
@Slf4j
public class CompanyJdbcAdapter implements CompanyRepository {

    private static final String PATH_GET_COMPANIES = "sql/getCompanies.sql";
    private static final String PATH_INSERT_COMPANY = "sql/insertCompany.sql";
    private static final String PATH_GET_COMPANY_BY_ID = "sql/getCompanyById.sql";
    private static final String PATH_UPDATE_COMPANY = "sql/updateCompany.sql";
    private static final String PATH_PATCH_COMPANY = "sql/patchCompany.sql";
    private static final String PATH_DELETE_COMPANY = "sql/deleteCompany.sql";
    private static final String COMPANY_ID = "COMPANY_ID";
    private static final String USER_ID = "USER_ID";
    private static final String NAME = "NAME";

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final String getCompaniesQuery;
    private final String insertCompaniesQuery;
    private final String getCompanyByIdQuery;
    private final String updateCompanyQuery;
    private final String patchCompanyQuery;
    private final String deleteCompanyQuery;

    public CompanyJdbcAdapter(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.getCompaniesQuery = SqlReader.readSql(PATH_GET_COMPANIES);
        this.insertCompaniesQuery = SqlReader.readSql(PATH_INSERT_COMPANY);
        this.getCompanyByIdQuery = SqlReader.readSql(PATH_GET_COMPANY_BY_ID);
        this.updateCompanyQuery = SqlReader.readSql(PATH_UPDATE_COMPANY);
        this.patchCompanyQuery = SqlReader.readSql(PATH_PATCH_COMPANY);
        this.deleteCompanyQuery = SqlReader.readSql(PATH_DELETE_COMPANY);
    }


    @Override
    public Company save(Company company) {
        Map<String, Object> params = new HashMap<>();
        params.put(USER_ID, company.getUserId());
        params.put(NAME, company.getName());

        printQuery(insertCompaniesQuery, params);
        Integer companyId = handleExceptions(() -> jdbcTemplate.queryForObject(insertCompaniesQuery, params, Integer.class));

        return company.withCompanyId(companyId);
    }


    @Override
    public void deleteById(Integer companyId) {
        final Map<String, Object> params = new HashMap<>();
        params.put(COMPANY_ID, companyId);

        printQuery(deleteCompanyQuery, params);
        handleExceptions(() -> jdbcTemplate.update(deleteCompanyQuery, params));
    }


    @Override
    public Company findById(Integer companyId) {
        final Map<String, Object> params = new HashMap<>();
        params.put(COMPANY_ID, companyId);

        printQuery(getCompanyByIdQuery, params);
        CompanyJdbcModel companyResult = handleExceptions(() -> jdbcTemplate.queryForObject(getCompanyByIdQuery, params, new BeanPropertyRowMapper<>(CompanyJdbcModel.class)));
        return companyResult.toDomain();
    }


    @Override
    public Company update(Company company) {
        final Map<String, Object> params = new HashMap<>();
        params.put(COMPANY_ID, company.getCompanyId());
        params.put(USER_ID, company.getUserId());
        params.put(NAME, company.getName());

        printQuery(updateCompanyQuery, params);
        handleExceptions(() -> jdbcTemplate.update(updateCompanyQuery, params));
        return company;
    }


    @Override
    public Page<Company> findAll(Integer page, Integer size, List<Filter> filters) {
        List<JdbcFilter> jdbcFilters = new JdbcFilterBuilder().build(filters);

        String countQuery = JdbcQueryBuilder.buildCountQuery(getCompaniesQuery, jdbcFilters);
        printQuery(countQuery, Collections.emptyMap());
        int totalElements = handleExceptions(() -> jdbcTemplate.queryForObject(countQuery, Collections.emptyMap(), Integer.class));
        int totalPages = totalElements == 0 ? 0 : 1 + (totalElements / size);

        String pageQuery = JdbcQueryBuilder.buildPagedSelectQuery(getCompaniesQuery, jdbcFilters, page, size);
        printQuery(pageQuery, Collections.emptyMap());
        List<CompanyJdbcModel> companiesResult = handleExceptions(() -> jdbcTemplate.query(pageQuery, Collections.emptyMap(), new BeanPropertyRowMapper<>(CompanyJdbcModel.class)));

        return Page.<Company>builder()
                .content(companiesResult.stream()
                        .map(CompanyJdbcModel::toDomain)
                        .collect(Collectors.toList()))
                .size(size)
                .number(page)
                .totalElements(totalElements)
                .totalPages(totalPages)
                .build();
    }


    @Override
    public List<Company> findAll(List<Filter> filters) {
        List<JdbcFilter> jdbcFilters = new JdbcFilterBuilder().build(filters);

        String selectQuery = JdbcQueryBuilder.buildSelectQuery(getCompaniesQuery, jdbcFilters);
        printQuery(selectQuery, Collections.emptyMap());
        List<CompanyJdbcModel> companiesResult = handleExceptions(() -> jdbcTemplate.query(selectQuery, Collections.emptyMap(), new BeanPropertyRowMapper<>(CompanyJdbcModel.class)));

        return companiesResult.stream()
                .map(CompanyJdbcModel::toDomain)
                .collect(Collectors.toList());
    }


    @Override
    public Company patch(Company company) {
        final Map<String, Object> params = new HashMap<>();
        params.put(COMPANY_ID, company.getCompanyId());
        params.put(USER_ID, company.getUserId());
        params.put(NAME, company.getName());

        printQuery(patchCompanyQuery, params);
        CompanyJdbcModel companyResult = handleExceptions(() -> jdbcTemplate.queryForObject(patchCompanyQuery, params, new BeanPropertyRowMapper<>(CompanyJdbcModel.class)));
        return companyResult.toDomain();
    }

    private void printQuery(String query, Map<String, Object> queryParams) {
        log.info("Executing query {} with params {}", query.replace("\r",""), queryParams);
    }

    private <T> T handleExceptions(Callable<T> callable) {
        try {
            return callable.call();
        } catch (EmptyResultDataAccessException ex) {
            log.error("Error finding Company", ex);
            throw new EntityNotFoundException(ErrorCode.ENTITY_NOT_FOUND);
        } catch (DataIntegrityViolationException ex) {
            log.error("Error saving Company", ex);
            throw new EntityConflictException(ErrorCode.REPOSITORY_CONFLICT);
        } catch (DataAccessException ex) {
            log.error("Unexpected error saving Company", ex);
            throw new RepositoryNotAvailableException(ErrorCode.REPOSITORY_NOT_AVAILABLE);
        } catch (Exception ex) {
            log.error("Unexpected error saving Company", ex);
            throw new RuntimeException("Unexpected error");
        }
    }
}