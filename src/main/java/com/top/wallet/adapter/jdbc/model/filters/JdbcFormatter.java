package com.top.wallet.adapter.jdbc.model.filters;

public interface JdbcFormatter {

    Object format(Object value);
}
