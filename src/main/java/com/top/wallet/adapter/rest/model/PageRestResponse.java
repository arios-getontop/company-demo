package com.top.wallet.adapter.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.top.wallet.domain.Page;
import lombok.Builder;
import lombok.Value;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Value
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class PageRestResponse<T> {
    List<T> content;
    int totalElements;
    int size;
    int number;
    int totalPages;

    public <U> Page<U> map(Function<T, U> convert) {
        return Page.<U>builder()
                .content(content.stream()
                        .map(convert)
                        .collect(Collectors.toList()))
                .totalElements(totalElements)
                .size(size)
                .number(number)
                .totalPages(totalPages)
                .build();
    }
}
