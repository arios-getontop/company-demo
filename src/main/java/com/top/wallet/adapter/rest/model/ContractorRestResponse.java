package com.top.wallet.adapter.rest.model;

import com.top.wallet.domain.Contractor;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ContractorRestResponse {

    private Integer contractorId;
    private String userId;
    private String name;

    public Contractor toDomain() {
        return Contractor.builder()
                .contractorId(contractorId)
                .userId(userId)
                .name(name)
                .build();
    }
}
