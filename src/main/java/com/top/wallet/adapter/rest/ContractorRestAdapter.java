package com.top.wallet.adapter.rest;

import com.top.wallet.adapter.rest.handler.RestTemplateErrorHandler;
import com.top.wallet.adapter.rest.model.ContractorRestResponse;
import com.top.wallet.adapter.rest.model.CreateContractorRequestModel;
import com.top.wallet.adapter.rest.model.PageRestResponse;
import com.top.wallet.application.exception.EntityConflictException;
import com.top.wallet.application.exception.EntityNotFoundException;
import com.top.wallet.application.exception.RepositoryBusinessException;
import com.top.wallet.application.exception.RepositoryNotAvailableException;
import com.top.wallet.application.port.out.ContractorRepository;
import com.top.wallet.config.ErrorCode;
import com.top.wallet.domain.Contractor;
import com.top.wallet.domain.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Component
public class ContractorRestAdapter implements ContractorRepository {

    private final RestTemplate restTemplate;
    private final String contractorsUrl;

    private static final String PAGE_PARAM = "page=0&size=10";
    private static final String CONTRACTOR_ID_KEY = "contractor_id";

    public ContractorRestAdapter(RestTemplate restTemplate,
                                 @Value("{services.contractors}") String contractorsUrl) {
        this.restTemplate = restTemplate;
        this.contractorsUrl = contractorsUrl;

        var errorHandler = new RestTemplateErrorHandler(
                Map.of(
                        HttpStatus.BAD_REQUEST, new RepositoryBusinessException(ErrorCode.REPOSITORY_BAD_REQUEST),
                        HttpStatus.NOT_FOUND, new EntityNotFoundException(ErrorCode.ENTITY_NOT_FOUND),
                        HttpStatus.CONFLICT, new EntityConflictException(ErrorCode.REPOSITORY_CONFLICT),
                        HttpStatus.INTERNAL_SERVER_ERROR, new RepositoryNotAvailableException(ErrorCode.REPOSITORY_NOT_AVAILABLE),
                        HttpStatus.SERVICE_UNAVAILABLE, new RepositoryNotAvailableException(ErrorCode.REPOSITORY_NOT_AVAILABLE)
                )
        );

        this.restTemplate.setErrorHandler(errorHandler);
    }


    @Override
    public Contractor save(Contractor contractor) {
        Integer contractorId = restTemplate.postForObject(contractorsUrl, CreateContractorRequestModel.fromDomain(contractor), Integer.class);
        return contractor.withContractorId(contractorId);
    }


    @Override
    public void deleteById(Integer contractorId) {
        final Map<String, String> queryParams = new HashMap<>();
        queryParams.put(CONTRACTOR_ID_KEY, String.valueOf(contractorId));

        restTemplate.delete(contractorsUrl, queryParams);
    }


    @Override
    public Page<Contractor> findAll(Integer page, Integer size, Map<String, String> filters) {
        ResponseEntity<PageRestResponse<ContractorRestResponse>> contractorsResponseEntity = restTemplate.exchange(
                contractorsUrl,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                },
                filters);

        PageRestResponse<ContractorRestResponse> pagedResponse = Optional.ofNullable(contractorsResponseEntity.getBody())
                .orElseThrow(() -> new RepositoryNotAvailableException(ErrorCode.REPOSITORY_NOT_AVAILABLE));

        return pagedResponse.map(ContractorRestResponse::toDomain);
    }


    @Override
    public Contractor findById(Integer contractorId) {
        final Map<String, String> queryParams = new HashMap<>();
        queryParams.put(CONTRACTOR_ID_KEY, String.valueOf(contractorId));

        ContractorRestResponse contractorResult = restTemplate.getForObject(contractorsUrl, ContractorRestResponse.class, queryParams);
        return contractorResult.toDomain();
    }

    @Override
    public Contractor patch(Contractor contractor) {
        return null;
    }

    @Override
    public Contractor update(Contractor contractor) {
        return null;
    }

    @Override
    public List<Contractor> findAll(Map<String, String> filters) {
        return null;
    }
}
