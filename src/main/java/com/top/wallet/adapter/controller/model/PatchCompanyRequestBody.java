package com.top.wallet.adapter.controller.model;

import com.top.wallet.config.ErrorCode;
import com.top.wallet.utils.ValidationRegex;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@AllArgsConstructor
@NoArgsConstructor
public class PatchCompanyRequestBody {

    @Pattern(regexp = ValidationRegex.UUID_REGEX, message = ErrorCode.Constants.INVALID_USER_ID)
    private String userId;
    @Size(min = 3, max = 255, message = ErrorCode.Constants.INVALID_NAME)
    private String name;
}
