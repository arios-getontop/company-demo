package com.top.wallet.domain;

import lombok.Builder;
import lombok.Value;
import lombok.With;

@Value
@Builder
public class Contractor {

    @With
    Integer contractorId;
    String userId;
    String name;
}