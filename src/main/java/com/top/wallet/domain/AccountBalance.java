package com.top.wallet.domain;

import lombok.Builder;
import lombok.Value;
import lombok.With;

import java.math.BigDecimal;

@Value
@Builder
public class AccountBalance {
    private BigDecimal balance;
}