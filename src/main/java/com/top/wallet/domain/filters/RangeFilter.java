package com.top.wallet.domain.filters;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
public class RangeFilter implements Filter {

    String key;
    String from;
    String to;

    @Override
    public Filter build(String key, List<String> values) {
        return new RangeFilterBuilder()
                .key(key)
                .from(getValue(values.get(0)))
                .to(values.size() > 1 ? getValue(values.get(1)) : null)
                .build();
    }

    private String getValue(String value) {
        return StringUtils.isEmpty(value) ? null : value;
    }
}
