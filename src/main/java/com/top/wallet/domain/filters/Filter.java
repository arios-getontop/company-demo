package com.top.wallet.domain.filters;

import java.util.List;

public interface Filter {

    Filter build(String key, List<String> values);
}
