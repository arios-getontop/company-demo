package com.top.wallet.domain.filters;

import com.top.wallet.application.exception.InvalidFiltersException;
import com.top.wallet.config.ErrorCode;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Slf4j
public class FilterBuilder {

    private final Map<String, Filter> filterFactory = Map.of(
            "range", new RangeFilter(),
            "equals", new EqualsFilter(),
            "in", new InListFilter(),
            "composite", new CompositeFilter(this),
            "contains", new ContainsFilter()
    );

    public List<Filter> build(Map<String, String> mappedFilters) {
        try {
            return doBuild(mappedFilters);
        } catch (IndexOutOfBoundsException ex) {
            log.error("Error de parseo de filtros", ex);
            throw new InvalidFiltersException(ErrorCode.INVALID_FILTERS);
        }
    }

    private List<Filter> doBuild(Map<String, String> mappedFilters) {
        List<Filter> filters = new ArrayList<>();
        mappedFilters.forEach((key, value) -> {
            List<String> valueSplit = Arrays.asList(value.split(","));
            Filter filter = buildFilter(key, valueSplit);
            filters.add(filter);
        });

        return filters;
    }

    public Filter buildFilter(String key, List<String> valueSplit) {
        String filterType;
        List<String> values;

        if(isComposite(valueSplit)) {
            filterType = "composite";
            values = valueSplit;
        }
        else {
            filterType = valueSplit.get(0);
            values = valueSplit.subList(1, valueSplit.size());
        }
        validate(filterType);
        return filterFactory.get(filterType).build(key, values);
    }

    private void validate(String filterType) {
        if (!filterFactory.containsKey(filterType)) {
            throw new InvalidFiltersException(ErrorCode.INVALID_FILTERS);
        }
    }

    private boolean isComposite(List<String> values) {
        return values.contains("or") || values.contains("and");
    }
}
