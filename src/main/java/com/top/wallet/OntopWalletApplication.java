package com.top.wallet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;


@EnableFeignClients
@SpringBootApplication
public class OntopWalletApplication {

	public static void main(String[] args) {
		SpringApplication.run(OntopWalletApplication.class, args);
	}

}
