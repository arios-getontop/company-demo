package com.top.wallet.application.usecase;

import com.top.wallet.application.exception.EntityNotFoundException;
import com.top.wallet.application.port.in.DeleteCompanyCommand;
import com.top.wallet.application.port.out.CompanyRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.temporal.ChronoUnit;

@Component
@Slf4j
public class DeleteCompanyUseCase implements DeleteCompanyCommand {

    private final CompanyRepository companyRepository;

    public DeleteCompanyUseCase(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }


    @Override
    public void execute(Data data) {
        try {
            companyRepository.deleteById(data.getCompanyId());
        } catch (EntityNotFoundException ignored) {
        }
    }
}
