package com.top.wallet.application.usecase;

import com.top.wallet.application.port.in.GetCompaniesListQuery;
import com.top.wallet.application.port.out.CompanyRepository;
import com.top.wallet.domain.Company;
import com.top.wallet.domain.filters.Filter;
import com.top.wallet.domain.filters.FilterBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.temporal.ChronoUnit;
import java.util.List;

@Component
@Slf4j
public class GetCompaniesListUseCase implements GetCompaniesListQuery {

    private final CompanyRepository companyRepository;

    public GetCompaniesListUseCase(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }


    @Override
    public List<Company> execute(Data data) {
        List<Filter> filters = new FilterBuilder().build(data.getFiltersMap());
        return companyRepository.findAll(filters);
    }
}
