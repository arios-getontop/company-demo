package com.top.wallet.application.usecase;

import com.top.wallet.application.port.in.UpdateCompanyCommand;
import com.top.wallet.application.port.out.CompanyRepository;
import com.top.wallet.domain.Company;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.temporal.ChronoUnit;

@Component
@Slf4j
public class UpdateCompanyUseCase implements UpdateCompanyCommand {

    private final CompanyRepository companyRepository;

    public UpdateCompanyUseCase(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }


    @Override
    public Company execute(Data data) {
        return companyRepository.update(data.toDomain());
    }
}