package com.top.wallet.application.usecase;

import com.top.wallet.application.port.in.GetCompaniesPageQuery;
import com.top.wallet.application.port.out.CompanyRepository;
import com.top.wallet.domain.Company;
import com.top.wallet.domain.Page;
import com.top.wallet.domain.filters.Filter;
import com.top.wallet.domain.filters.FilterBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.temporal.ChronoUnit;
import java.util.List;

@Component
@Slf4j
public class GetCompaniesPageUseCase implements GetCompaniesPageQuery {

    private final CompanyRepository companyRepository;

    public GetCompaniesPageUseCase(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }


    @Override
    public Page<Company> execute(Data data) {
        List<Filter> filters = new FilterBuilder().build(data.getFiltersMap());
        return companyRepository.findAll(data.getPage(), data.getSize(), filters);
    }
}