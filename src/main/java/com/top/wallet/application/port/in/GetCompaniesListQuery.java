package com.top.wallet.application.port.in;

import com.top.wallet.domain.Company;
import lombok.Builder;
import lombok.Value;

import java.util.List;
import java.util.Map;

public interface GetCompaniesListQuery {

    List<Company> execute(Data data);

    @Value
    @Builder
    class Data {
        Map<String, String> filtersMap;
    }
}
