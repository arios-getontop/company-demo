package com.top.wallet.application.port.in;

import com.top.wallet.domain.Page;
import com.top.wallet.domain.Company;
import lombok.Builder;
import lombok.Value;

import java.util.Map;

public interface GetCompaniesPageQuery {

    Page<Company> execute(Data data);

    @Value
    @Builder
    class Data {
        Integer page;
        Integer size;
        Map<String, String> filtersMap;
    }
}
