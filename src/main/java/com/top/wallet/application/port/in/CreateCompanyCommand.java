package com.top.wallet.application.port.in;

import com.top.wallet.domain.Company;
import lombok.Builder;
import lombok.Value;

public interface CreateCompanyCommand {

    Company execute(Data data);

    @Value
    @Builder
    class Data {
        String userId;
        String name;

        public Company toDomain() {
            return Company.builder()
                .userId(userId)
                .name(name)
                .build();
        }
    }
}

