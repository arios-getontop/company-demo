package com.top.wallet.application.port.out;

import com.top.wallet.domain.Contractor;
import com.top.wallet.domain.Page;
import com.top.wallet.domain.filters.Filter;

import java.util.List;
import java.util.Map;

public interface ContractorRepository {

    Contractor save(Contractor contractor);
    void deleteById(Integer contractorId);
    List<Contractor> findAll(Map<String, String> filters);
    Page<Contractor> findAll(Integer page, Integer size, Map<String, String> filters);
    Contractor findById(Integer contractorId);
    Contractor patch(Contractor contractor);
    Contractor update(Contractor contractor);
}
