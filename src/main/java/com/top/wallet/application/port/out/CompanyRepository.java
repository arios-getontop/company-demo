package com.top.wallet.application.port.out;

import com.top.wallet.domain.Page;
import com.top.wallet.domain.filters.Filter;
import com.top.wallet.domain.Company;

import java.util.List;

public interface CompanyRepository {

    Company save(Company company);
    void deleteById(Integer companyId);
    List<Company> findAll(List<Filter> filters);
    Page<Company> findAll(Integer page, Integer size, List<Filter> filters);
    Company findById(Integer companyId);
    Company patch(Company company);
    Company update(Company company);
}
