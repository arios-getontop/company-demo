package com.top.wallet.application.port.in;

import com.top.wallet.domain.Company;
import lombok.Builder;
import lombok.Value;

public interface PatchCompanyCommand {

    Company execute(Data data);

    @Value
    @Builder
    class Data {
        Integer companyId;
        String userId;
        String name;

        public Company toDomain() {
            return Company.builder()
                .companyId(companyId)
                .userId(userId)
                .name(name)
                .build();
        }
    }
}
