package com.top.wallet.application.exception;

import com.top.wallet.config.ErrorCode;
import org.springframework.http.HttpStatus;

public class RepositoryBusinessException extends HttpErrorException {

    public RepositoryBusinessException(ErrorCode errorCode) {
        super(HttpStatus.BAD_REQUEST.value(), errorCode);
    }
}
