package com.top.wallet.application.exception;

import com.top.wallet.config.ErrorCode;
import org.springframework.http.HttpStatus;

public class EntityNotFoundException extends HttpErrorException {

    public EntityNotFoundException(ErrorCode errorCode) {
        super(HttpStatus.NOT_FOUND.value(), errorCode);
    }
}
