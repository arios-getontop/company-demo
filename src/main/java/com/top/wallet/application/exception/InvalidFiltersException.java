package com.top.wallet.application.exception;

import com.top.wallet.config.ErrorCode;

public class InvalidFiltersException extends BusinessException {
    public InvalidFiltersException(ErrorCode errorCode) {
        super(errorCode);
    }
}