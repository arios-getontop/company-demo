package com.top.wallet.config;

public enum ErrorCode {

    REPOSITORY_BAD_REQUEST(Constants.REPOSITORY_BAD_REQUEST),
    ENTITY_NOT_FOUND(Constants.ENTITY_NOT_FOUND),
    REPOSITORY_CONFLICT(Constants.REPOSITORY_CONFLICT),
    REPOSITORY_NOT_AVAILABLE(Constants.REPOSITORY_NOT_AVAILABLE),
    ENTITY_ALREADY_EXISTS(Constants.ENTITY_ALREADY_EXISTS),
    INVALID_FILTERS(Constants.INVALID_FILTERS),
    INVALID_USER_ID(Constants.INVALID_USER_ID),
    INVALID_NAME(Constants.INVALID_NAME);

    ErrorCode(String value) {
    }

    public static class Constants {
        public static final String REPOSITORY_BAD_REQUEST = "REPOSITORY_BAD_REQUEST";
        public static final String ENTITY_NOT_FOUND = "ENTITY_NOT_FOUND";
        public static final String REPOSITORY_CONFLICT = "REPOSITORY_CONFLICT";
        public static final String REPOSITORY_NOT_AVAILABLE = "REPOSITORY_NOT_AVAILABLE";
        public static final String ENTITY_ALREADY_EXISTS = "ENTITY_ALREADY_EXISTS";
        public static final String INVALID_FILTERS = "INVALID_FILTERS";
        public static final String INVALID_USER_ID = "INVALID_USER_ID";
        public static final String INVALID_NAME = "INVALID_NAME";
    }
}
