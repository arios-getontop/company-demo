package com.top.wallet.config;

import com.top.wallet.application.exception.BusinessException;
import com.top.wallet.application.exception.EntityConflictException;
import com.top.wallet.application.exception.EntityNotFoundException;
import com.top.wallet.application.exception.RepositoryNotAvailableException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@ControllerAdvice
public class ErrorHandler {

    @ExceptionHandler({BusinessException.class})
    public ResponseEntity handle(BusinessException ex) {
        log.error("Error handled : ", ex);
        return ResponseEntity.status(ex.getErrorResponse().getHttpStatus()).body(ex.getErrorResponse());
    }

    @ExceptionHandler({EntityNotFoundException.class})
    public ResponseEntity handle(EntityNotFoundException ex) {
        log.error("Error handled : ", ex);
        return ResponseEntity.status(ex.getErrorResponse().getHttpStatus()).body(ex.getErrorResponse());
    }

    @ExceptionHandler({EntityConflictException.class})
    public ResponseEntity handle(EntityConflictException ex) {
        log.error("Error handled : ", ex);
        return ResponseEntity.status(ex.getErrorResponse().getHttpStatus()).body(ex.getErrorResponse());
    }

    @ExceptionHandler({RepositoryNotAvailableException.class})
    public ResponseEntity handle(RepositoryNotAvailableException ex) {
        log.error("Error handled : ", ex);
        return ResponseEntity.status(ex.getErrorResponse().getHttpStatus()).body(ex.getErrorResponse());
    }

    @ExceptionHandler({MethodArgumentTypeMismatchException.class})
    public ResponseEntity handle(MethodArgumentTypeMismatchException ex) {
        log.error("Bad argument", ex);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
            .build();
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ResponseEntity handle(MethodArgumentNotValidException ex) {
        log.error("Bad argument", ex);
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(camelToSnake(fieldName), errorMessage);
        });
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
            .body(errors);
    }



    @ExceptionHandler({Throwable.class})
    public ResponseEntity handle(Throwable ex) {
        log.error("Unexpected Error", ex);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    private static String camelToSnake(String str) {
        return str.replaceAll("([a-z])([A-Z]+)", "$1_$2").toLowerCase();
    }
}

