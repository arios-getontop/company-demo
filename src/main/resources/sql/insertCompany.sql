INSERT INTO companies (user_id, name)
VALUES (:USER_ID, :NAME)
RETURNING company_id;
